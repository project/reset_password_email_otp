<?php

/**
 * @file
 * Hooks and documentation related to the Reset Password Email OTP module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Session\AccountInterface;

/**
 * Alter the recipient phone number.
 *
 * @param string $phone_number
 *   The phone number to send the SMS to.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account of the recipient.
 */
function hook_reset_password_otp_sms_recipient_alter(string &$phone_number, AccountInterface $account): void {
  // Example to convert all mobile numbers in the UK standard to +44 prefix.
  // All UK mobile numbers start with digits in the range 071-075 or 077-079
  // except the Isle of Man which starts with 076.
  // They look like 07911123456.
  $phone_number = str_replace(' ', '', $phone_number);
  if (str_starts_with($phone_number, '07') && strlen($phone_number) === 10) {
    $phone_number = '+44' . ltrim($phone_number, '0');
  }
  elseif (str_starts_with($phone_number, '7') && strlen($phone_number) === 10) {
    $phone_number = '+44' . ltrim($phone_number, '0');
  }
}

/**
 * @} End of "addtogroup hooks".
 */
