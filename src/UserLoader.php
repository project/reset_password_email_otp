<?php

namespace Drupal\reset_password_email_otp;

use Drupal\user\UserInterface;

/**
 * Service that loads the user account.
 */
class UserLoader {

  /**
   * Load the user entity.
   *
   * This is done via a service to allow 3rd parties to override it.
   *
   * @param string $email_or_username
   *   The email or username to search for.
   *
   * @return \Drupal\user\UserInterface|false
   *   The user entity or false.
   *
   * @see https://www.drupal.org/project/reset_password_email_otp/issues/3490984
   */
  public function loadUser(string $email_or_username): UserInterface|FALSE {
    $account = user_load_by_mail($email_or_username);
    if ($account instanceof UserInterface) {
      return $account;
    }

    $account = user_load_by_name($email_or_username);
    if ($account instanceof UserInterface) {
      return $account;
    }
    return FALSE;
  }

}
