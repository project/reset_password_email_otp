<?php

namespace Drupal\reset_password_email_otp;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Twilio\Exceptions\TwilioException;

/**
 * Service that handles OTP data.
 */
class OtpManager {
  use StringTranslationTrait;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   Messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactoryChannel
   *   Logger.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\Component\Datetime\TimeInterface $timeService
   *   The datetime.time service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(
    protected Connection $connection,
    protected Messenger $messenger,
    protected LoggerChannelFactoryInterface $loggerFactoryChannel,
    protected ConfigFactory $configFactory,
    protected MailManagerInterface $mailManager,
    protected TimeInterface $timeService,
    protected ModuleHandlerInterface $moduleHandler,
    protected Token $token,
  ) {
    $this->loggerFactory = $loggerFactoryChannel->get('reset_password_email_otp');
  }

  /**
   * Get a new OTP passcode.
   *
   * @param array $settings
   *   The module configuration.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return string
   *   The passcode.
   */
  public function getPasscode(array $settings, AccountInterface $account): string {
    $this->cleanOldPasscodes();

    // Generate a unique passcode and save it.
    $otp = $this->generatePasscode($settings);
    $this->saveOtp($account->id(), $otp);
    return $otp;
  }

  /**
   * Delete OTPs older than 1 hour.
   */
  public function cleanOldPasscodes(): void {
    $one_hour_ago = $this->timeService->getRequestTime() - 3600;
    $this->connection->delete('reset_password_email_otp_list')
      ->condition('time', $one_hour_ago, '<')
      ->execute();
  }

  /**
   * Get reset OTP.
   *
   * @param array $settings
   *   String limit OTP.
   *
   * @return string
   *   OTP return.
   */
  public function generatePasscode(array $settings): string {
    $token = "";
    $options = $settings['format_options'];
    if (in_array('exclude_ambiguous', $options, TRUE)) {
      if (in_array('include_uppercase_letters', $options, TRUE)) {
        $codeAlphabet = "ABCDEFGHJKMNPQRSTUVWXYZ";
      }
      if (in_array('include_lowercase_letters', $options, TRUE)) {
        $codeAlphabet .= "abcdefghjkmnpqrstuvwxyz";
      }
      if (in_array('include_numbers', $options, TRUE)) {
        $codeAlphabet .= "23456789";
      }
    }
    else {
      if (in_array('include_uppercase_letters', $options, TRUE)) {
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      }
      if (in_array('include_lowercase_letters', $options, TRUE)) {
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
      }
      if (in_array('include_numbers', $options, TRUE)) {
        $codeAlphabet .= "0123456789";
      }
    }
    if (in_array('include_symbols', $options, TRUE)) {
      $codeAlphabet .= "!@#$%^&";
    }
    $max = strlen($codeAlphabet);

    // Generate a random passcode similar to how Core's Default Password
    // Generator generate method behaves.
    for ($i = 0; $i < $settings['reset_password_email_otp_length']; $i++) {
      $token .= $codeAlphabet[random_int(0, $max - 1)];
    }

    // Ensure OTP is unique, otherwise try again.
    $result = $this->connection->select('reset_password_email_otp_list', 'otp')
      ->fields('otp', ['resetpass_id'])
      ->condition('OTP', $token)
      ->range(0, 1)
      ->execute()
      ->fetchField();
    if ($result) {
      return $this->generatePasscode($settings);
    }
    return $token;
  }

  /**
   * Save sent OTP to the database.
   *
   * @param int $uid
   *   User uid.
   * @param int $otp
   *   Random OTP.
   */
  public function saveOtp($uid, $otp) {
    $data = [
      'uid' => $uid,
      'OTP' => $otp,
      'count' => 0,
      'time' => $this->timeService->getRequestTime(),
    ];
    $this->connection->insert('reset_password_email_otp_list')
      ->fields($data)
      ->execute();

    return TRUE;
  }

  /**
   * Send an OTP email.
   *
   * @param string $otp
   *   The one time passcode.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return bool
   *   Whether the email was sent or not.
   */
  public function sendEmail(
    string $otp,
    AccountInterface $account,
  ): bool {
    $settings = $this->configFactory->get('reset_password_email_otp.settings');
    $params['account'] = $account;
    $params['subject'] = $settings->get('reset_password_email_otp_mail_subject');
    $params['message'] = str_replace('[OTP]', $otp, $settings->get('reset_password_email_otp_mail_body'));
    $language_code = $account->getPreferredLangcode();

    // Send mail to the given user.
    $email = $account->getEmail();
    if (!empty($email)) {
      $module = 'reset_password_email_otp';
      $key = 'reset-password-email-otp';
      $result = $this->mailManager->mail($module, $key, $email, $language_code, $params);
      if ($result['result'] !== TRUE) {
        $message = $this->t('There was a problem sending your email notification to @email.', [
          '@email' => $email,
        ]);
        $this->messenger->addError($message);
        $this->loggerFactory->error($message);
        return FALSE;
      }
      $message = $this->t('An email notification with OTP has been sent to @email', [
        '@email' => $email,
      ]);
      $this->loggerFactory->notice($message);
      return TRUE;
    }

    $message = $this->t('Unable to send an OTP email to the provided account User ID @account', [
      '@account' => $account->id(),
    ]);
    $this->loggerFactory->notice($message);
    return FALSE;
  }

  /**
   * Send SMS callback.
   *
   * @param string $otp
   *   The one time passcode.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return bool
   *   Whether the SMS was sent or not.
   */
  public function sendSms(string $otp, AccountInterface $account): bool {
    if (class_exists('Twilio\Rest\Client')) {
      $settings = $this->configFactory->get('reset_password_email_otp.settings')->getRawData();

      // Ignore the non-use statement loading since dependency may not exist.
      // @codingStandardsIgnoreStart
      // @phpstan-ignore-next-line
      $token = \Drupal::service('key.repository')->getKey($settings['twilio_token'])->getKeyValue();
      // @codingStandardsIgnoreEnd

      // Get the phone number from the specified source field.
      $phone_number = '';
      if (
        $account instanceof FieldableEntityInterface
        && $account->hasField($settings['sms_mobile_number_field'])
        && !$account->get($settings['sms_mobile_number_field'])->isEmpty()
      ) {
        $phone_number = $account->get($settings['sms_mobile_number_field'])->getString();
      }

      // Allow other modules to alter the phone number being sent to Twilio.
      $this->moduleHandler->alter(
        'reset_password_otp_sms_recipient',
        $phone_number,
        $account,
      );
      if (!$phone_number) {
        return FALSE;
      }

      // Ignore the non-use statement loading since dependency may not exist.
      // @codingStandardsIgnoreLine
      $twilio = new \Twilio\Rest\Client($settings['twilio_account_id'], $token);
      try {
        $settings['sms_body'] = $this->token->replace($settings['sms_body'], [
          'user' => $account,
        ]);
        $settings['sms_body'] = str_replace('\n', "\n", $settings['sms_body']);
        $twilio->messages->create($phone_number, [
          'from' => $settings['sms_sending_mobile_number'],
          'body' => str_replace('[OTP]', $otp, $settings['sms_body']),
        ]);
        $message = $this->t('An SMS notification with OTP has been sent to the mobile number of account @email', [
          '@email' => $account->getEmail(),
        ]);
        $this->loggerFactory->notice($message);
        return TRUE;
      }
      catch (TwilioException $exception) {
        $message = $this->t('Failure to send an SMS OTP with error message: @message', [
          '@message' => $exception->getMessage(),
        ]);
        $this->loggerFactory->warning($message);
      }
    }
    else {
      $message = $this->t('Class for Twilio "Twilio\Rest\Client" does not exist');
      $this->loggerFactory->warning($message);
    }
    return FALSE;
  }

}
