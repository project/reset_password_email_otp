<?php

namespace Drupal\reset_password_email_otp\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure reset password email otp settings.
 */
class ResetPasswordMailOTPConfig extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'reset_password_email_otp.settings';

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected ModuleHandlerInterface $moduleHandler,
    protected EntityFieldManagerInterface $entityFieldManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * Helper to get the config keys.
   *
   * @return string[]
   *   The configuration keys.
   */
  public static function getConfigKeys(): array {
    return [
      'reset_password_email_otp_wrong_attempt',
      'format_options',
      'reset_password_email_otp_length',
      'step_one_title',
      'step_one_email_or_username_label',
      'step_one_email_or_username_description',
      'step_one_sending_preference',
      'step_one_next',
      'step_two_title',
      'step_two_otp_label',
      'step_two_otp_description',
      'step_two_back',
      'step_two_next',
      'step_three_title',
      'step_three_password_description',
      'step_three_back',
      'step_three_submit',
      'step_four_login',
      'sending_option',
      'reset_password_email_otp_mail_subject',
      'reset_password_email_otp_mail_body',
      'twilio_account_id',
      'twilio_token',
      'sms_body',
      'sms_sending_mobile_number',
      'sms_mobile_number_field',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reset_password_email_otp_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['reset_password_email_otp_wrong_attempt'] = [
      '#title' => $this->t('Wrong attempt OTP limit'),
      '#type' => 'textfield',
      '#description' => $this->t('How many time user can attempt wrong OTP.'),
      '#default_value' => $config->get('reset_password_email_otp_wrong_attempt'),
    ];

    $form['format'] = [
      '#type' => 'details',
      '#title' => $this->t('OTP Format'),
    ];
    $form['format']['format_options'] = [
      '#title' => $this->t('Options'),
      '#type' => 'checkboxes',
      '#options' => [
        'include_lowercase_letters' => $this->t('Include lowercase letters'),
        'include_uppercase_letters' => $this->t('Include uppercase letters'),
        'include_numbers' => $this->t('Include numbers'),
        'include_symbols' => $this->t('Include symbols'),
        'exclude_ambiguous' => $this->t('Exclude ambiguous characters and numbers (L, O, I, 1)'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('format_options') ?? [],
      '#description' => $this->t('The inclusion options will be factored in to the random generation of the code; however, it may not be that all of the above exist in a generated code. Use the "Exclude ambiguous" option when using letters and numbers to avoid similar looking characters like L and O. An OTP is valid for 1 hour from generation time sending.'),
    ];
    $form['format']['reset_password_email_otp_length'] = [
      '#title' => $this->t('Number of characters'),
      '#type' => 'textfield',
      '#description' => $this->t('The total number of characters.'),
      '#default_value' => $config->get('reset_password_email_otp_length'),
    ];

    $form['step_one'] = [
      '#type' => 'details',
      '#title' => $this->t('Step one'),
    ];
    $form['step_one']['step_one_title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#description' => $this->t('This is the heading 2 appearing above the form step 1.'),
      '#default_value' => $config->get('step_one_title'),
    ];
    $form['step_one']['step_one_email_or_username_label'] = [
      '#title' => $this->t('Email or username field label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_one_email_or_username_label'),
      '#required' => TRUE,
    ];
    $form['step_one']['step_one_email_or_username_description'] = [
      '#title' => $this->t('Email or username field description'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_one_email_or_username_description'),
    ];
    $form['step_one']['step_one_sending_preference'] = [
      '#title' => $this->t('Sending preference'),
      '#type' => 'textfield',
      '#maxlength' => 500,
      '#default_value' => $config->get('step_one_sending_preference'),
      '#description' => $this->t('If you have configured SMS, a suggested text is: "By choosing SMS as your preferred method, if your account has a valid mobile number stored, a one-time passcode will be sent there. If no valid mobile number is stored, an email will be sent instead anyways.". This field is not shown to the user if SMS is not configured.'),
    ];
    $form['step_one']['step_one_next'] = [
      '#title' => $this->t('Next button label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_one_next'),
      '#required' => TRUE,
    ];

    $form['step_two'] = [
      '#type' => 'details',
      '#title' => $this->t('Step two'),
    ];
    $form['step_two']['step_two_title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#description' => $this->t('This is the heading 2 appearing above the form step 2.'),
      '#default_value' => $config->get('step_two_title'),
    ];
    $form['step_two']['step_two_otp_label'] = [
      '#title' => $this->t('One time passcode label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_two_otp_label'),
      '#required' => TRUE,
    ];
    $form['step_two']['step_two_otp_description'] = [
      '#title' => $this->t('One time passcode field description'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_two_otp_description'),
    ];
    $form['step_two']['step_two_back'] = [
      '#title' => $this->t('Back button label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_two_back'),
      '#required' => TRUE,
    ];
    $form['step_two']['step_two_next'] = [
      '#title' => $this->t('Next button label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_two_next'),
      '#required' => TRUE,
    ];

    $form['step_three'] = [
      '#type' => 'details',
      '#title' => $this->t('Step three'),
    ];
    $form['step_three']['step_three_title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#description' => $this->t('This is the heading 2 appearing above the form step 3.'),
      '#default_value' => $config->get('step_three_title'),
    ];
    $form['step_three']['step_three_password_description'] = [
      '#title' => $this->t('Password field description'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_three_password_description'),
    ];
    $form['step_three']['step_three_back'] = [
      '#title' => $this->t('Back button label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_three_back'),
      '#required' => TRUE,
    ];
    $form['step_three']['step_three_submit'] = [
      '#title' => $this->t('Submit button label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_three_submit'),
      '#required' => TRUE,
    ];

    $form['step_four'] = [
      '#type' => 'details',
      '#title' => $this->t('Step four'),
    ];
    $form['step_four']['step_four_login'] = [
      '#title' => $this->t('Login button label'),
      '#type' => 'textfield',
      '#default_value' => $config->get('step_four_login'),
      '#required' => TRUE,
    ];

    $form['sending_option'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sending option'),
      '#options' => [
        'prefer_email' => $this->t('Default to email if multiple options are set up'),
        'prefer_sms' => $this->t('Default to SMS if multiple options are set up'),
      ],
      '#default_value' => $config->get('sending_option') ?: 'prefer_email',
    ];

    $form['email'] = [
      '#type' => 'details',
      '#title' => $this->t('Email'),
    ];
    $form['email']['reset_password_email_otp_mail_subject'] = [
      '#title' => $this->t('Reset Mail OTP mail subject'),
      '#type' => 'textfield',
      '#description' => $this->t("Reset Password Mail OTP mail subject content.<br>The following tokens are supported: [user:display-name], [user:account-name], [user:mail]"),
      '#default_value' => $config->get('reset_password_email_otp_mail_subject'),
    ];
    $form['email']['reset_password_email_otp_mail_body'] = [
      '#title' => $this->t('Reset Password Mail OTP mail body'),
      '#type' => 'textarea',
      '#description' => $this->t("Make sure to use [OTP] in email body for contains OTP value.<br>The following tokens are supported: [user:display-name], [user:account-name], [user:mail]"),
      '#default_value' => $config->get('reset_password_email_otp_mail_body'),
    ];

    $form['sms'] = [
      '#type' => 'details',
      '#title' => $this->t('SMS'),
    ];
    $form['sms']['details'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Filling in an SMS option will give the user the choice of having an SMS sent with their OTP. If their mobile number is unknown, the email will be sent instead'),
    ];
    $warnings = [];
    if (!$this->moduleHandler->moduleExists('key')) {
      $warnings[] = $this->t('Please install the "key" module to enter a Twilio API key <code>composer require drupal/key</code>.');
    }
    if (!class_exists('Twilio\Rest\Client')) {
      $warnings[] = $this->t('Please install the Twilio PHP library <code>composer require twilio/sdk</code>.');
    }
    if ($warnings) {
      $form['sms']['warnings'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => $warnings,
        ],
      ];
    }
    else {
      $form['sms']['twilio_account_id'] = [
        '#type' => 'textfield',
        '#default_value' => $config->get('twilio_account_id'),
        '#title' => $this->t('Twilio Account SID'),
        '#description' => $this->t('Your account SID and token can be found at <a href="@link" target="_blank">@link</a>', [
          '@link' => 'https://console.twilio.com/',
        ]),
      ];
      $form['sms']['twilio_token'] = [
        '#type' => 'key_select',
        '#default_value' => $config->get('twilio_token'),
        '#title' => $this->t('Twilio Account Token'),
      ];
      $form['sms']['sms_sending_mobile_number'] = [
        '#type' => 'textfield',
        '#default_value' => $config->get('sms_sending_mobile_number'),
        '#title' => $this->t('Sender mobile number'),
        '#description' => $this->t('This must be a mobile number registered in the Twilio dashboard as a sending number.'),
      ];
      $form['sms']['sms_body'] = [
        '#type' => 'textarea',
        '#default_value' => $config->get('sms_body'),
        '#title' => $this->t('SMS Body'),
        '#description' => $this->t("Make sure to use [OTP] in the SMS body. It will be automatically replaced with the OTP.<br>Keep the maximum length <i>including</i> estimated token replacements to 300 characters or less.<br>Line breaks are supported.<br>The following tokens are also available: [user:display-name], and [user:account-name]."),
        // The maximum recommended length by Twilio is 320 characters. A shorter
        // length makes it quite safe that with the OTP itself there are no
        // length issues.
        '#maxlength' => 255,
      ];
      $definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
      $options = [];
      foreach ($definitions as $key => $definition) {
        $options[$key] = $definition->getLabel();
      }
      $form['sms']['sms_mobile_number_field'] = [
        '#type' => 'select',
        '#default_value' => $config->get('sms_mobile_number_field'),
        '#title' => $this->t('User mobile number field'),
        '#description' => $this->t("Select the field on the user that contains the valid mobile number that the SMS should be sent to. If blank, an email will be sent instead. The mobile number must validate with Twilio's library, you can do your own modifications with <code>hook_reset_password_otp_sms_recipient_alter</code>. If Twilio throws an exception for an invalid number, you will find it in the Drupal logs."),
        '#options' => $options,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!str_contains($values['reset_password_email_otp_mail_body'], '[OTP]')) {
      $form_state->setErrorByName('reset_password_email_otp_mail_body', $this->t('Please add [OTP] text into the Email body.'));
    }
    if (!empty($values['sms_body']) && !str_contains($values['sms_body'], '[OTP]')) {
      $form_state->setErrorByName('sms_body', $this->t('Please add [OTP] text into the SMS body.'));
    }

    // If any required SMS fields are filled in, all must be filled in.
    $sms_required_fields = [
      'twilio_account_id',
      'twilio_token',
      'sms_sending_mobile_number',
      'sms_body',
      'sms_mobile_number_field',
    ];
    $filled = [];
    foreach ($sms_required_fields as $sms_required_field) {
      if (!empty($values[$sms_required_field])) {
        $filled[] = $sms_required_field;
      }
    }
    if ($filled && count($filled) !== count($sms_required_fields)) {
      foreach (array_diff($sms_required_fields, $filled) as $not_filled_field) {
        $form_state->setErrorByName($not_filled_field, $this->t('When configuring Twilio, you must set all related values'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('reset_password_email_otp.settings');
    // Save form values.
    foreach ($this->getConfigKeys() as $key) {
      $config->set($key, $form_state->getValue($key))->save();
    }

    Cache::invalidateTags(['reset_password_email_otp']);

    parent::submitForm($form, $form_state);
  }

}
