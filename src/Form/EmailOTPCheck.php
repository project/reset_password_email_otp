<?php

namespace Drupal\reset_password_email_otp\Form;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reset_password_email_otp\OtpManager;
use Drupal\reset_password_email_otp\UserLoader;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class for reset password email otp.
 */
class EmailOTPCheck extends FormBase {

  /**
   * The reset password email OTP configuration settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Class Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   Connection.
   * @param \Drupal\reset_password_email_otp\OtpManager $otpManager
   *   The OTP manager service.
   * @param \Drupal\reset_password_email_otp\UserLoader $userLoader
   *   The user loader service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected Connection $connection,
    protected OtpManager $otpManager,
    protected UserLoader $userLoader,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('reset_password_email_otp.otp_manager'),
      $container->get('reset_password_email_otp.user_loader'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reset_password_email_otp_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->config = $this->config('reset_password_email_otp.settings');
    if ($form_state->has('step_num') && $form_state->get('step_num') == 2) {
      return $this->fapiOtpVerificationPageTwo($form, $form_state);
    }

    if ($form_state->has('step_num') && $form_state->get('step_num') == 3) {
      return $this->fapiSetPasswordPageThree($form, $form_state);
    }

    if ($form_state->has('step_num') && $form_state->get('step_num') == 4) {
      return $this->fapiSetPasswordPageFour($form, $form_state);
    }

    $form_state->set('step_num', 1);

    if ($this->config->get('step_one_title')) {
      $form['description'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->config->get('step_one_title'),
        '#attributes' => [
          'class' => [
            'otp-description',
            'otp-description--identify-account',
          ],
        ],
      ];
    }

    $form['email_or_username'] = [
      '#type' => 'textfield',
      '#title' => $this->config->get('step_one_email_or_username_label'),
      '#default_value' => $form_state->getValue('email_or_username', ''),
      '#required' => TRUE,
    ];
    if ($this->config->get('step_one_email_or_username_description')) {
      $form['email_or_username']['#description'] = $this->config->get('step_one_email_or_username_description');
    }

    if ($this->smsEnabled()) {
      $form['sending_preference'] = [
        '#type' => 'radios',
        '#options' => [
          'sms' => $this->t('Text message (SMS)'),
          'email' => $this->t('Email'),
        ],
        '#default_value' => $this->config->get('sending_option') === 'prefer_email' ? 'email' : 'sms',
      ];
      if ($this->config->get('step_one_sending_preference')) {
        $form['sending_preference']['#description'] = $this->config->get('step_one_sending_preference');
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->config->get('step_one_next'),
      '#submit' => ['::fapiStepOneNextSubmit'],
    ];

    return $form;
  }

  /**
   * Check whether SMS is enabled.
   *
   * @return bool
   *   If SMS is enabled.
   */
  protected function smsEnabled(): bool {
    if (
      $this->config->get('twilio_account_id')
      && $this->config->get('twilio_token')
      && $this->config->get('sms_sending_mobile_number')
      && $this->config->get('sms_mobile_number_field')
      && class_exists('Twilio\Rest\Client')
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->fapiStepThreeSubmit($form, $form_state);
  }

  /**
   * Rebuild the form so any validation errors can be shown.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form.
   */
  public function ajaxStepTwoRebuild(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Rebuild the form so any validation errors can be shown.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form.
   */
  public function ajaxStepThreeRebuild(array &$form, FormStateInterface $form_state) {
    return $form['wrapper'];
  }

  /**
   * Provides custom submission handler for step 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepOneNextSubmit(array &$form, FormStateInterface $form_state) {
    $email_or_username = $form_state->getValue('email_or_username');
    $account = $this->userLoader->loadUser($email_or_username);
    if ($account instanceof UserInterface && $account->isActive()) {
      $sent = FALSE;

      // Generate and save the passcode.
      $settings = $this->configFactory->get('reset_password_email_otp.settings')->getRawData();
      $otp = $this->otpManager->getPasscode($settings, $account);

      // Send via SMS if that is preferred.
      if ($form_state->getValue('sending_preference') === 'sms') {
        $sent = $this->otpManager->sendSms($otp, $account);
      }

      // If SMS is either not preferred or failed, send via email.
      if ($sent === FALSE) {
        $this->otpManager->sendEmail($otp, $account);
      }
    }

    // Make sure the status text is displayed even if no email was sent. This
    // message is deliberately the same as the success message for privacy.
    if ($form_state->getValue('sending_preference') === 'sms') {
      $this->messenger()->addStatus($this->t('If %identifier is a valid account and has a mobile number, an SMS will have been sent with a one time passcode (OTP). If no mobile number is found, an email will have been sent with OTP to reset your password.', [
        '%identifier' => $form_state->getValue('email_or_username'),
      ]));
    }
    else {
      $this->messenger()->addStatus($this->t('If %identifier is a valid account, an email will be sent with a one time passcode (OTP) to reset your password.', [
        '%identifier' => $form_state->getValue('email_or_username'),
      ]));
    }

    $form_state
      ->set('page_values', [
        'email_or_username' => $form_state->getValue('email_or_username'),
      ])
      ->set('step_num', 2)
      ->setRebuild();
  }

  /**
   * Builds the second step form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function fapiOtpVerificationPageTwo(array &$form, FormStateInterface $form_state) {

    if ($this->config->get('step_two_title')) {
      $form['description'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->config->get('step_two_title'),
        '#attributes' => [
          'class' => [
            'otp-description',
            'otp-description--verification',
          ],
        ],
      ];
    }

    $form['otp'] = [
      '#type' => 'textfield',
      '#title' => $this->config->get('step_two_otp_label'),
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('otp', ''),
    ];
    if ($this->config->get('step_two_otp_description')) {
      $form['otp']['#description'] = $this->config->get('step_two_otp_description');
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#button_type' => 'secondary',
      '#value' => $this->config->get('step_two_back'),
      '#submit' => ['::fapiStepTwoBack'],
      '#limit_validation_errors' => [],
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->config->get('step_two_next'),
      '#validate' => ['::fapiStepTwoFormNextValidate'],
      '#ajax' => [
        'callback' => '::ajaxStepTwoRebuild',
        'event' => 'click',
        'wrapper' => 'reset-password-email-otp-form',
      ],
      '#submit' => ['::fapiStepTwoNextSubmit'],
    ];

    return $form;
  }

  /**
   * Provides custom validation handler for page 2.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepTwoFormNextValidate(array &$form, FormStateInterface $form_state) {
    $otp = $form_state->getValue('otp');
    $page_values = $form_state->get('page_values');
    $email_or_username = $page_values['email_or_username'];
    $account = $this->userLoader->loadUser($email_or_username);
    if ($account instanceof UserInterface) {
      $this->otpManager->cleanOldPasscodes();

      // Get database connection to get has detail.
      $query = $this->connection->select('reset_password_email_otp_list', 'email_otp')
        ->fields('email_otp', ['resetpass_id', 'uid', 'time', 'OTP', 'count'])
        ->condition('uid', $account->id(), '=')
        ->range(0, 1)
        ->orderBy('time', 'DESC');
      $user_record = $query->execute()->fetchAssoc();
      // Get config limit of OTP wrong attempt.
      $limit_wrong_otp = $this->configFactory->get('reset_password_email_otp.settings')
        ->get('reset_password_email_otp_wrong_attempt');
      if ($user_record !== FALSE && $otp !== $user_record['OTP']) {
        if ($user_record['count'] < $limit_wrong_otp) {
          $form_state->setErrorByName('otp', $this->t('The OTP is not valid. Please try again.'));
          $this->connection->update('reset_password_email_otp_list')
            ->fields([
              'count' => $user_record['count'] + 1,
            ])
            ->condition('resetpass_id', $user_record['resetpass_id'], '=')
            ->execute();
        }
        else {
          $form_state->setErrorByName('otp', $this->t('You have tried maximum number of limit, please try after sometime.'));
        }
      }
    }
    else {
      $form_state->setErrorByName('otp', $this->t('The OTP is not valid. Please try again.'));
    }
  }

  /**
   * Provides custom submission handler for step 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepTwoNextSubmit(array &$form, FormStateInterface $form_state) {
    $page_values = $form_state->get('page_values');
    $form_state
      ->set('page_values', [
        'email_or_username' => $page_values['email_or_username'],
        'otp' => $form_state->getValue('otp'),
      ])
      ->set('step_num', 3)
      ->setRebuild();
  }

  /**
   * Provides custom submission handler for 'Back' button (step 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepTwoBack(array &$form, FormStateInterface $form_state) {
    $form_state
      ->setValues($form_state->get('page_values'))
      ->set('step_num', 1)
      ->setRebuild(TRUE);
  }

  /**
   * Builds the third step form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function fapiSetPasswordPageThree(array &$form, FormStateInterface $form_state) {
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => [
          $this->getFormId() . '--page-three',
        ],
      ],
    ];

    if ($this->config->get('step_three_title')) {
      $form['wrapper']['description'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->config->get('step_three_title'),
        '#attributes' => [
          'class' => [
            'otp-description',
            'otp-description--reset-password',
          ],
        ],
      ];
    }

    $form['wrapper']['confirm_pass'] = [
      '#type' => 'password_confirm',
      '#size' => 32,
      '#required' => TRUE,
    ];
    if ($this->config->get('step_three_password_description')) {
      $form['wrapper']['confirm_pass']['#description'] = $this->config->get('step_three_password_description');
    }

    $form['wrapper']['actions'] = [
      '#type' => 'actions',
    ];

    $form['wrapper']['actions']['back'] = [
      '#type' => 'submit',
      '#button_type' => 'secondary',
      '#value' => $this->config->get('step_three_back'),
      '#submit' => ['::fapiStepThreeBack'],
      '#limit_validation_errors' => [],
    ];

    $form['wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->config->get('step_three_submit'),
      '#validate' => ['::fapiStepThreeValidate'],
      '#ajax' => [
        'callback' => '::ajaxStepThreeRebuild',
        'event' => 'click',
        'wrapper' => $this->getFormId() . '--page-three',
      ],
      '#submit' => ['::fapiStepThreeSubmit'],
    ];

    return $form;
  }

  /**
   * Builds the third step form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function fapiSetPasswordPageFour(array &$form, FormStateInterface $form_state) {
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => [
          $this->getFormId() . '--page-four',
        ],
      ],
    ];

    $login_url = Url::fromRoute('user.login');
    $form['wrapper']['message'] = [
      '#theme' => 'status_messages',
      '#message_list' => [
        'status' => [
          $this->t("Your password has been successfully changed. You may proceed to <a href='@login'>the login area.</a>", [
            '@login' => $login_url->toString(),
          ]),
        ],
      ],
      '#attributes' => [
        'class' => [
          'otp-messages',
        ],
      ],
    ];

    $form['wrapper']['link'] = [
      '#type' => 'link',
      '#url' => $login_url,
      '#title' => $this->t('Go to login'),
      '#attributes' => [
        'class' => [
          'button',
          'button--primary',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Rebuild the form so any validation errors can be shown.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form.
   */
  public function ajaxStepRebuild(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Provides custom submission handler for 'Back' button (step 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepThreeBack(array &$form, FormStateInterface $form_state) {
    $form_state
      ->setValues($form_state->get('page_values'))
      ->set('step_num', 2)
      ->setRebuild();
  }

  /**
   * Re-validate the final step to ensure nothing has changed.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepThreeValidate(array &$form, FormStateInterface $form_state) {
    $otp = $form_state->getValue('otp');
    $page_values = $form_state->get('page_values');
    $email_or_username = $page_values['email_or_username'];
    $account = $this->userLoader->loadUser($email_or_username);
    if ($account instanceof UserInterface) {

      // Find an exact match for the account and OTP.
      $matches = $this->connection->select('reset_password_email_otp_list', 'email_otp')
        ->fields('email_otp', ['resetpass_id', 'uid', 'time', 'OTP', 'count'])
        ->condition('uid', $account->id())
        ->condition('OTP', $otp)
        ->range(0, 1)
        ->orderBy('time', 'DESC')
        ->execute()
        ->fetchAll();
      if (!empty($matches)) {
        $form_state->setErrorByName('otp', $this->t('An attempt to change the target account was made. This is forbidden.'));
      }
    }
    else {
      $form_state->setErrorByName('confirm_pass', $this->t('An attempt to change the target account was made. This is forbidden.'));
    }
  }

  /**
   * Submit handler for saving the password.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function fapiStepThreeSubmit(array &$form, FormStateInterface $form_state) {
    $page_values = $form_state->get('page_values');
    $email_or_username = $page_values['email_or_username'];
    $account = $this->userLoader->loadUser($email_or_username);
    if ($account instanceof UserInterface) {
      $account->setPassword($form_state->getValue('confirm_pass'));
      $account->save();
    }

    $form_state->setValues($form_state->get('page_values'))
      ->set('step_num', 4)
      ->setRebuild();
  }

}
