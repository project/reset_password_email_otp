<?php

namespace Drupal\Tests\reset_password_email_otp\FunctionalJavascript;

use Drupal\Component\Utility\Random;
use Drupal\Core\Database\Database;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\key\Entity\Key;
use Drupal\user\UserInterface;

/**
 * Contains Reset Password Email OTP functionality tests.
 *
 * @group reset_password_email_otp
 */
class ResetPasswordSmsOtpTest extends WebDriverTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reset_password_email_otp',
    'reset_password_email_otp_sms_test',
    'user',
    'node',
    'system',
    'block',
    'dblog',
    'field',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User 1 for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser1;

  /**
   * User 2 for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser2;

  /**
   * Admin 1 for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $admin1;

  /**
   * A random string for keeping a unique run.
   *
   * @var string
   */
  protected string $random;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('reset_password_email_otp_form');
    $this->random = (new Random())->string();

    FieldStorageConfig::create([
      'field_name' => 'field_mobile_number',
      'type' => 'text',
      'entity_type' => 'user',
      'cardinality' => 1,
    ])->save();
    FieldConfig::create([
      'entity_type' => 'user',
      'field_name' => 'field_mobile_number',
      'bundle' => 'user',
      'label' => 'Mobile number',
    ])->save();
    $this->testUser1 = $this->createUser([], 'test1', FALSE, [
      'mail' => 'test1@test.com',
      'field_mobile_number' => '07812341234',
    ]);
    $this->testUser2 = $this->createUser([], 'test2', FALSE, [
      'mail' => 'test2@test.com',
    ]);
    $this->admin1 = $this->createUser([], 'admin1', TRUE, [
      'mail' => 'admin@test.com',
    ]);
  }

  /**
   * Tests the reset password process for an existent account.
   */
  public function testResetPasswordSms() {
    // No SMS yet configured.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextNotContains('Text message (SMS)');

    $this->drupalLogin($this->admin1);

    // Set up fake key.
    Key::create([
      'id' => 'twilio_token_key_id',
      'label' => 'Twilio token key ID',
      'key_type' => 'authentication',
      'key_provider' => 'config',
      'key_provider_settings' => [
        'key_value' => '1234',
      ],
    ])->save();

    // Configure SMS.
    $this->drupalGet('admin/config/people/reset-password-email-otp');
    $this->getSession()->getPage()->findById('edit-sms')->click();
    $this->submitForm([
      'sending_option' => 'prefer_sms',
      'twilio_account_id' => '1234',
      'twilio_token' => 'twilio_token_key_id',
      'sms_sending_mobile_number' => '1234',
      'sms_mobile_number_field' => 'field_mobile_number',
    ], 'Save configuration');
    $this->drupalLogout();
    drupal_flush_all_caches();

    // Send SMS attempt, expect failure.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Text message (SMS)');
    $this->assertSession()->elementAttributeContains('css', '#edit-sending-preference-sms', 'checked', 'checked');
    $this->submitForm([
      'email_or_username' => 'test1@test.com',
    ], 'Next');
    $this->assertSession()->waitForText('If test1@test.com is a valid account and has a mobile number, an SMS will have been sent with a one time passcode (OTP). If no mobile number is found, an email will have been sent with OTP to reset your password.');

    // Check the logs for a failure.
    $messages = Database::getConnection()->select('watchdog', 'w')
      ->fields('w', ['message'])
      ->condition('type', 'reset_password_email_otp')
      ->orderBy('wid', 'DESC')
      ->range(0, 2)
      ->execute()
      ->fetchCol();
    $this->assertStringContainsString('An email notification with OTP has been sent to test1@test.com', reset($messages));
    $this->assertStringContainsString('Failure to send an SMS OTP with error message', end($messages));

    // Get OTP.
    $connection = Database::getConnection();
    $query = $connection->select('reset_password_email_otp_list', 'r');
    $query->addField('r', 'resetpass_id');
    $query->addField('r', 'OTP');
    $results = $query->execute()->fetchAll();
    $this->assertCount(1, $results);
    $result = reset($results);
    $otp = $result->OTP;
    $this->assertSession()->waitForId('edit-otp');

    $this->submitForm([
      'otp' => $otp,
    ], 'Next');
    $this->assertSession()->waitForText('Reset Password');

    $this->assertSession()->waitForId('edit-confirm-pass-pass1');
    $this->submitForm([
      'confirm_pass[pass1]' => 'test1',
      'confirm_pass[pass2]' => 'test1',
    ], 'Submit');

    $this->assertSession()->waitForText('Your password has been successfully changed.');
    $this->assertSession()->pageTextContains('Go to login');

    // Clean up OTPs.
    $connection->delete('reset_password_email_otp_list')->execute();
  }

}
