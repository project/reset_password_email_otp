<?php

namespace Drupal\Tests\reset_password_email_otp\FunctionalJavascript;

use Drupal\Component\Utility\Random;
use Drupal\Core\Database\Database;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\UserInterface;

/**
 * Contains Reset Password Email OTP functionality tests.
 *
 * @group reset_password_email_otp
 */
class ResetPasswordEmailOtpTest extends WebDriverTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reset_password_email_otp',
    'user',
    'system',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User 1 for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser1;

  /**
   * User 2 for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser2;

  /**
   * A random string for keeping a unique run.
   *
   * @var string
   */
  protected string $random;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('reset_password_email_otp_form');
    $this->random = (new Random())->string();
    $this->testUser1 = $this->createUser([], 'test1', FALSE, [
      'mail' => 'test1@test.com',
    ]);
    $this->testUser2 = $this->createUser([], 'test2', FALSE, [
      'mail' => 'test2@test.com',
    ]);
  }

  /**
   * Tests the reset password for non-existent accounts.
   */
  public function testResetPasswordNotExisting() {
    $this->drupalGet('<front>');
    $this->submitForm([
      'email_or_username' => 'not-existing@test.com',
    ], 'Next');

    // Expect to OTPs added for an invalid account.
    $connection = Database::getConnection();
    $query = $connection->select('reset_password_email_otp_list', 'r');
    $query->addField('r', 'resetpass_id');
    $results = $query->execute()->fetchAll();
    $this->assertCount(0, $results);
  }

  /**
   * Tests the reset password process OTP validation fail.
   */
  public function testResetPasswordValidationFail() {
    $this->drupalGet('<front>');
    $this->submitForm([
      'email_or_username' => 'test1@test.com',
    ], 'Next');

    // Expect to OTPs added for an invalid account.
    $connection = Database::getConnection();
    $query = $connection->select('reset_password_email_otp_list', 'r');
    $query->addField('r', 'resetpass_id');
    $query->addField('r', 'OTP');
    $results = $query->execute()->fetchAll();
    $this->assertCount(1, $results);

    // Expect to have a validation error.
    $this->assertSession()->waitForElementVisible('css', '.js-form-item-otp input');
    $this->submitForm([
      'otp' => 'wrong-otp-' . $this->random,
    ], 'Next');

    // Expect to have a validation error.
    $this->assertSession()->waitForText('The OTP is not valid. Please try again.');

    // Clean up OTPs.
    $connection->delete('reset_password_email_otp_list')->execute();
  }

  /**
   * Tests the reset password process for an existent account.
   */
  public function testResetPasswordExisting() {
    $this->drupalGet('<front>');
    $this->submitForm([
      'email_or_username' => 'test1@test.com',
    ], 'Next');

    // Expect to OTPs added for an invalid account.
    $connection = Database::getConnection();
    $query = $connection->select('reset_password_email_otp_list', 'r');
    $query->addField('r', 'resetpass_id');
    $query->addField('r', 'OTP');
    $results = $query->execute()->fetchAll();
    $this->assertCount(1, $results);
    $result = reset($results);
    $otp = $result->OTP;
    $this->assertSession()->waitForId('edit-otp');

    $this->submitForm([
      'otp' => $otp,
    ], 'Next');
    $this->assertSession()->waitForText('Reset Password');

    $this->assertSession()->waitForId('edit-confirm-pass-pass1');
    $this->submitForm([
      'confirm_pass[pass1]' => 'test1',
      'confirm_pass[pass2]' => 'test1',
    ], 'Submit');

    $this->assertSession()->waitForText('Your password has been successfully changed.');
    $this->assertSession()->pageTextContains('Go to login');

    // Clean up OTPs.
    $connection->delete('reset_password_email_otp_list')->execute();
  }

}
