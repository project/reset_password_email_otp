<?php

namespace Drupal\Tests\reset_password_email_otp\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Contains Reset Password Email OTP settings tests.
 *
 * @group reset_password_email_otp
 */
class ResetPasswordEMailOtpSettingsTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'reset_password_email_otp',
    'user',
    'system',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $testUser;

  /**
   * User for settings management.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('reset_password_email_otp_form');
    $this->testUser = $this->createUser([], 'test1', FALSE, [
      'mail' => 'test1@test.com',
    ]);
    $this->adminUser = $this->createUser([], 'admin1', TRUE);
  }

  /**
   * Tests the settings management.
   */
  public function testChangeLabel() {
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Identify your account');

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/people/reset-password-email-otp');
    $this->submitForm([
      'step_one_title' => 'Changed step one title',
    ], 'Save configuration');
    $this->drupalLogout();

    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Changed step one title');
  }

}
